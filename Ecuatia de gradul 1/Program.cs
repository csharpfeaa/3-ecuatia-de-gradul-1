﻿using System;

namespace Ecuatia_de_gradul_1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Introduceti a: ");

            if (!double.TryParse(Console.ReadLine(), out double a))
            {
                Console.WriteLine("Numarul introdus nu este valid.");
                Console.ReadKey();
                return;
            }

            Console.Write("Introduceti b: ");

            if (!double.TryParse(Console.ReadLine(), out double b))
            {
                Console.WriteLine("Numarul introdus nu este valid.");
                Console.ReadKey();
                return;
            }

            if (a == 0)
            {
                Console.WriteLine("Ecuatie imposibila");
            }
            else
            {
                Console.WriteLine($"Solutia ecuatiei {a}x + {b} = 0 este: {-b/a}");
            }

            Console.ReadKey();
        }
    }
}